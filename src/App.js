import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "./firebase";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import PrincipalPanel from "./components/principalPanel/principalPanel";
import NewClient from "./components/newClient/newClient";
import EditClient from "./components/editClient/editClient";
import AddSession from "./components/addSession/addSession";
import UserSingin from "./components/userSingin/userSingin";
import EditSession from "./components/editSession/editSession";

import { setUserLogin } from "./actions/generalActions";

import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component {
  componentDidMount() {
    firebase.auth().onAuthStateChanged((authenticated) => {
      if (authenticated) {
        console.log("logged");
        this.props.setUserLogin();
      }
    });
  }

  render() {
    return (
      <Router>
        <Switch>
          <Redirect exact from="/" to="usersingin" />
          {this.props.generalReducer.userLogged && (
            <Redirect exact from="/" to="usersingin" />
          )}
          <Route exact path="/usersingin">
            {!this.props.generalReducer.userLogged ? (
              <UserSingin />
            ) : (
              <Redirect to="principalpanel" />
            )}
          </Route>
          <Route exact path="/principalpanel">
            {this.props.generalReducer.userLogged ? (
              <PrincipalPanel />
            ) : (
              <Redirect to="usersingin" />
            )}
          </Route>
          <Route exact path="/editclient">
            {this.props.generalReducer.userLogged ? (
              Object.keys(this.props.generalReducer.selectedClient).length !==
              0 ? (
                <EditClient />
              ) : (
                <Redirect exact from="/" to="principalpanel" />
              )
            ) : (
              <Redirect to="usersingin" />
            )}
          </Route>
          <Route exact path="/newclient">
            {this.props.generalReducer.userLogged ? (
              <NewClient />
            ) : (
              <Redirect to="usersingin" />
            )}
          </Route>
          <Route exact path="/editsession">
            {this.props.generalReducer.userLogged ? (
              this.props.generalReducer.selectedSession !== "" ? (
                <EditSession />
              ) : (
                <PrincipalPanel />
              )
            ) : (
              <Redirect to="usersingin" />
            )}
          </Route>
          <Route exact path="/addsession">
            {this.props.generalReducer.userLogged ? (
              Object.keys(this.props.generalReducer.selectedClient).length !==
              0 ? (
                <AddSession />
              ) : (
                <Redirect exact from="/" to="principalpanel" />
              )
            ) : (
              <Redirect to="usersingin" />
            )}
          </Route>
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
  };
};

App.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  setUserLogin: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { setUserLogin })(App);
