import firebase from "firebase";

const config = {
  apiKey: "AIzaSyCy98VhypNiGfSL7QxloKs1kBBLWneDRkc",
  authDomain: "annaarmengolestetica-be3d5.firebaseapp.com",
  databaseURL: "https://annaarmengolestetica-be3d5.firebaseio.com",
  projectId: "annaarmengolestetica-be3d5",
  storageBucket: "annaarmengolestetica-be3d5.appspot.com",
  messagingSenderId: "787212343939",
  appId: "1:787212343939:web:967785187404d20c5699b6",
  measurementId: "G-R05GLTBZJ2",
};

firebase.initializeApp(config);
export default firebase;
