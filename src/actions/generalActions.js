import * as actionTypes from "../actions/actions";

export const clientInformation = (clientInfo) => {
  return {
    type: actionTypes.CLIENT_INFO,
    payload: clientInfo,
  };
};

export const editClientInformation = (clientInformation) => {
  return {
    type: actionTypes.EDIT_CLIENT_INFO,
    payload: clientInformation,
  };
};

export const editSessionInfo = (sessionId) => {
  return {
    type: actionTypes.EDIT_SESSION_INFO,
    payload: sessionId,
  };
};

export const setUserLogin = () => {
  return {
    type: actionTypes.USER_LOGGED,
  };
};

export const setUserLogout = () => {
  return {
    type: actionTypes.USER_LOGOUT,
  };
};

export const setClients = (clients) => {
  return {
    type: actionTypes.SET_CLIENTS,
    payload: clients,
  };
};

export const setHairThickness = (hairThickness) => {
  return {
    type: actionTypes.SET_HAIR_THICKNESS,
    payload: hairThickness,
  };
};

export const setSkinTypes = (skinTypes) => {
  return {
    type: actionTypes.SET_SKIN_TYPES,
    payload: skinTypes,
  };
};

export const setTanDegrees = (tanDegrees) => {
  return {
    type: actionTypes.SET_TAN_DEGREES,
    payload: tanDegrees,
  };
};

export const setZones = (zones) => {
  return {
    type: actionTypes.SET_ZONES,
    payload: zones,
  };
};
