import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "../../firebase";
import logo from "../../img/logo.png";

import { setUserLogin } from "../../actions/generalActions";

export class UserSingin extends Component {
  state = {
    userName: "",
    password: "",
    error: false,
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleOnUserLogin = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(this.state.userName, this.state.password)
      .then(() => {
        this.props.setUserLoggin();
        this.setState({
          error: false,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({ error: true });
      });
  };

  render() {
    return (
      <div className="container">
        <div className="col-12 mt-5">
          <div className="row justify-content-center">
            <img src={logo} alt="logo" className="w-7" />
          </div>
          <div className="row mt-3">
            <div className="col-12">
              <h3 className="text-center">Iniciar sessió</h3>
              {this.state.error && (
                <h5 className="text-danger text-center mt-4 mb-3">
                  {" "}
                  Usuari o contrasenya incorrecte
                </h5>
              )}
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Email:</div>
            <div className="col-10 mt-3">
              <input
                type="text"
                name="userName"
                onChange={this.handleOnChange}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">
              Contrasenya:
            </div>
            <div className="col-10 mt-3">
              <input
                type="password"
                name="password"
                onChange={this.handleOnChange}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12 text-center mt-4">
              <button
                className="btn btn-primary"
                onClick={this.handleOnUserLogin}
              >
                Iniciar Sessió
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
  };
};

UserSingin.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  setUserLogin: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { setUserLogin })(UserSingin);
