import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";
import Swal from "sweetalert2";
import firebase from "../../firebase";

import { editClientInformation } from "../../actions/generalActions";
import { formValidation } from "../../validation/formValidation/formValidation";

import "../../generalStyle.css";
import "./editClient.css";

class EditClient extends Component {
  state = {
    name: "",
    phone: "",
    email: "",
    observations: "",
    loaded: false,
    edited: false,
    nameValue: true,
    phoneValue: true,
    emailValue: true,
    observationsValue: true,
    infoServerOK: false,
  };

  componentDidMount() {
    this.setState({
      name: this.props.generalReducer.selectedClient.Name,
      phone: this.props.generalReducer.selectedClient.Phone,
      email: this.props.generalReducer.selectedClient.Email,
      observations: this.props.generalReducer.selectedClient.Observations,
      loaded: this.props.generalReducer.selectedClient.hasOwnProperty("Name"),
    });
  }

  componentDidUpdate() {
    if (
      !this.state.loaded &&
      this.state.name === "" &&
      this.state.name !== this.props.generalReducer.selectedClient.Name &&
      this.state.phone === "" &&
      this.state.phone !== this.props.generalReducer.selectedClient.Phone &&
      this.state.email === "" &&
      this.state.email !== this.props.generalReducer.selectedClient.Email &&
      this.state.observations === "" &&
      this.state.observations !==
        this.props.generalReducer.selectedClient.Observations
    ) {
      this.setState({
        name: this.props.generalReducer.selectedClient.Name,
        phone: this.props.generalReducer.selectedClient.Phone,
        email: this.props.generalReducer.selectedClient.Email,
        observations: this.props.generalReducer.selectedClient.Observations,
        loaded: true,
      });
    }
  }

  handleOnClickEditClientData = () => {
    let that = this;
    let clientInfomation = {
      name: this.state.name,
      phoneNumber: this.state.phone,
      email: this.state.email,
      observations: this.state.observations,
    };
    this.props.editClientInformation(clientInfomation);
    firebase
      .firestore()
      .collection("CLIENTS")
      .doc(this.props.generalReducer.selectedClient.clientId)
      .update({
        Email: this.state.email,
        Name: this.state.name,
        Observations: this.state.observations,
        Phone: this.state.phone,
      })
      .then(() => {
        that.setState({
          edited: true,
        });
        Swal.fire({
          icon: "success",
          title: "Client modificat amb èxit",
          showConfirmButton: false,
          timer: 3000,
        });
      })
      .catch(() => {
        Swal.fire({
          icon: "error",
          title: "Error en modifcar el client",
          showConfirmButton: false,
          timer: 3000,
        });
      });
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
    this.validateData(e.target.name, e.target.value);
  };

  validateData = (name, value) => {
    if (!formValidation(name, value)) {
      this.setState({
        [name + "Value"]: false,
      });
    } else {
      this.setState({
        [name + "Value"]: true,
      });
    }
  };

  render() {
    return (
      <div className="container">
        <div className="col-12 mt-5">
          <div className="row">
            {this.state.edited && <Redirect to="principalpanel" />}
            <div className="col-12">
              <h3 className="text-center">Informació client/a</h3>
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Nom:</div>
            <div className="col-10 mt-3">
              <input
                type="text"
                name="name"
                value={this.state.name}
                onChange={this.handleOnChange}
                className={
                  this.state.nameValue ? "form-control" : "form-control"
                }
              />
            </div>
          </div>
          {/*<div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Teléfon:</div>
            <div className="col-10 mt-3">
              <input
                type="text"
                name="phone"
                value={this.state.phone}
                onChange={this.handleOnChange}
                className={
                  this.state.phoneValue ? "form-control" : "form-control"
                }
              />
            </div>
              </div>*/}
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Mail:</div>
            <div className="col-10 mt-3">
              <input
                type="email"
                name="email"
                value={this.state.email}
                onChange={this.handleOnChange}
                className={
                  this.state.emailValue ? "form-control" : "form-control"
                }
              />
            </div>
          </div>

          <div className="row">
            <p className="col-12 mt-4">observacions:</p>
          </div>
          <div className="row">
            <div className="col-12">
              <textarea
                name="observations"
                value={this.state.observations}
                onChange={this.handleOnChange}
                className={
                  this.state.observationsValue ? "form-control" : "form-control"
                }
              />
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-6 text-center mt-4">
              <Link to="principalpanel">
                <button className="btn btn-danger">Cancel·lar</button>
              </Link>
            </div>
            <div className="col-6 text-center mt-4">
              <button
                className="btn btn-primary"
                onClick={this.handleOnClickEditClientData}
              >
                Confirmar
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
  };
};

EditClient.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  editClientInformation: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { editClientInformation })(EditClient);
