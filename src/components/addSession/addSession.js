import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";
import Swal from "sweetalert2";
import firebase from "../../firebase";

import {
  setHairThickness,
  setSkinTypes,
  setTanDegrees,
  setZones,
} from "../../actions/generalActions";

import "../../generalStyle.css";
import "./addSession.css";

class AddSession extends Component {
  state = {
    skinType: "",
    hairThickness: "",
    tanDegree: "",
    zone: "",
    date: "",
    energy: "",
    shoots: "",
    sessionAdded: false,
  };

  componentDidMount() {
    let dbTables = ["HAIRTHICKNESS", "SKINTYPES", "TANDEGREES", "ZONES"];
    let dbTablesNames = ["HairThickness", "SkinTypes", "TanDegrees", "Zones"];
    let aux = [];
    let that = this;
    for (let i in dbTables) {
      firebase
        .firestore()
        .collection(dbTables[i])
        .get()
        .then(function (docs) {
          let auxArray = [];
          docs.forEach((doc) => {
            aux = doc.data();
            aux[`${dbTablesNames[i]}Id`] = doc.id;
            auxArray.push(aux);
          });
          // Segons a la taula de la BD que mirem executem una funcio o una altre
          if (dbTables[i] === "HAIRTHICKNESS") {
            auxArray = that.sortArray(auxArray);
            that.props.setHairThickness(auxArray);
            that.setState({
              hairThickness: auxArray[0].Description,
            });
          }
          if (dbTables[i] === "SKINTYPES") {
            auxArray = that.sortArray(auxArray);
            that.props.setSkinTypes(auxArray);
            that.setState({
              skinType: auxArray[0].Description,
            });
          }
          if (dbTables[i] === "TANDEGREES") {
            auxArray = that.sortArray(auxArray);
            that.props.setTanDegrees(auxArray);
            that.setState({
              tanDegree: auxArray[0].Description,
            });
          }
          if (dbTables[i] === "ZONES") {
            auxArray = that.sortArray(auxArray);
            that.props.setZones(auxArray);
            that.setState({
              zone: auxArray[0].Description,
            });
          }
        });
    }
  }

  sortArray = (array) => {
    array.sort((a, b) => {
      if (a.Description < b.Description) return -1;
      if (a.Description > b.Description) return 1;
      return 0;
    });

    return array;
  };

  componentDidUpdate() {}

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleOnClickAddSession = () => {
    firebase
      .firestore()
      .collection("SESSIONS")
      .add({
        ClientId: this.props.generalReducer.selectedClient.clientId,
        HairThikness: this.state.hairThickness,
        SkinType: this.state.skinType,
        TanDegree: this.state.tanDegree,
        Zone: this.state.zone,
        Date: this.state.date,
        Energy: this.state.energy,
        ShootNumber: this.state.shoots,
      })
      .then(() => {
        Swal.fire({
          icon: "success",
          title: "Sessió afegida amb èxit",
          showConfirmButton: false,
          timer: 3000,
        });
        this.setState({
          sessionAdded: true,
        });
      })
      .catch(() => {
        Swal.fire({
          icon: "error",
          title: "Error al afegir la sessió",
          showConfirmButton: false,
          timer: 3000,
        });
      });
  };

  render() {
    return (
      <div className="container">
        <div className="col-12 mt-5">
          <div className="row">
            {this.state.sessionAdded && <Redirect to="principalpanel" />}
            <div className="col-12">
              <h3 className="text-center">
                Afegir Sessió a {this.props.generalReducer.selectedClient.Name}
              </h3>
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">
              Tipus de pell:
            </div>
            <div className="col-10 mt-3">
              <select
                className="custom-select"
                name="skinType"
                onChange={this.handleOnChange}
              >
                {this.props.generalReducer.skinTypes.map((skinType, key) => {
                  return (
                    <option key={key} value={skinType.Description}>
                      {skinType.Description}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">
              Grau de broncejat:
            </div>
            <div className="col-10 mt-3">
              <select
                className="custom-select"
                name="tanDegree"
                onChange={this.handleOnChange}
              >
                {this.props.generalReducer.tanDegrees.map((tanDegree, key) => {
                  return (
                    <option key={key} value={tanDegree.tanDegreesId}>
                      {tanDegree.Description}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">
              Tipus de pèl:
            </div>
            <div className="col-10 mt-3">
              <select
                className="custom-select"
                name="hairThickness"
                onChange={this.handleOnChange}
              >
                {this.props.generalReducer.hairThikness.map(
                  (hairThikness, key) => {
                    return (
                      <option key={key} value={hairThikness.HairThiknessId}>
                        {hairThikness.Description}
                      </option>
                    );
                  }
                )}
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Zona:</div>
            <div className="col-10 mt-3">
              <select
                className="custom-select"
                name="zone"
                onChange={this.handleOnChange}
              >
                {this.props.generalReducer.zones.map((zone, key) => {
                  return (
                    <option key={key} value={zone.zonesId}>
                      {zone.Description}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Data:</div>
            <div className="col-10 mt-3">
              <input
                type="date"
                name="date"
                value={this.state.date}
                onChange={this.handleOnChange}
                className={
                  this.state.nameValue ? "form-control" : "form-control"
                }
              />
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Energia:</div>
            <div className="col-10 mt-3">
              <input
                type="text"
                name="energy"
                value={this.state.energy}
                onChange={this.handleOnChange}
                className={
                  this.state.nameValue ? "form-control" : "form-control"
                }
              />
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">
              Número de trets:
            </div>
            <div className="col-10 mt-3">
              <input
                type="number"
                name="shoots"
                value={this.state.shoots}
                onChange={this.handleOnChange}
                className={
                  this.state.nameValue ? "form-control" : "form-control"
                }
              />
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-6 text-center mt-4">
              <Link to="principalpanel">
                <button className="btn btn-danger">Cancel·lar</button>
              </Link>
            </div>
            <div className="col-6 text-center mt-4">
              <button
                className="btn btn-primary"
                onClick={this.handleOnClickAddSession}
              >
                Confirmar
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
  };
};

AddSession.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  setHairThickness: PropTypes.func.isRequired,
  setSkinTypes: PropTypes.func.isRequired,
  setTanDegrees: PropTypes.func.isRequired,
  setZones: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  setHairThickness,
  setSkinTypes,
  setTanDegrees,
  setZones,
})(AddSession);
