import React, { Component } from "react";
import { Table } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "../../firebase";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faEdit } from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";

import { editSessionInfo } from "../../actions/generalActions";

import "antd/dist/antd.css";
import { Redirect } from "react-router-dom";

class TaulaSessions extends Component {
  state = {
    selectedClientId: "",
    columns: "",
    sessionsData: [],
    editSession: false,
  };

  columns = [
    {
      title: "DATA",
      dataIndex: "Date",
    },
    {
      title: "PELL",
      dataIndex: "SkinType",
      sorter: (a, b) => {
        return a.SkinType > b.SkinType ? -1 : b.SkinType > a.SkinType ? 1 : 0;
      },
    },
    {
      title: "BRONCEJAT",
      dataIndex: "TanDegree",
      sorter: (a, b) => {
        return a.TanDegree > b.TanDegree
          ? -1
          : b.TanDegree > a.TanDegree
          ? 1
          : 0;
      },
    },
    {
      title: "PÈL",
      dataIndex: "HairThikness",
      sorter: (a, b) => {
        return a.HairThikness > b.HairThikness
          ? -1
          : b.HairThikness > a.HairThikness
          ? 1
          : 0;
      },
    },
    {
      title: "ZONA",
      dataIndex: "Zone",
      sorter: (a, b) => {
        return a.TanDegree > b.TanDegree
          ? -1
          : b.TanDegree > a.TanDegree
          ? 1
          : 0;
      },
    },
    {
      title: "ENERGIA",
      dataIndex: "Energy",
      sorter: (a, b) => a.Energy - b.Energy,
    },
    {
      title: "DISPARS",
      dataIndex: "ShootNumber",
      sorter: (a, b) => a.ShootNumber - b.ShootNumber,
    },
    {
      title: "",
      dataIndex: "",
      render: (field) => (
        <div className="col-12">
          <div className="row">
            <div className="col-6">
              <FontAwesomeIcon
                icon={faEdit}
                onClick={() => this.editSession(field)}
              />
            </div>
            <div className="col-6">
              <FontAwesomeIcon
                icon={faTrashAlt}
                onClick={() => this.deleteSession(field)}
              />
            </div>
          </div>
        </div>
      ),
    },
  ];

  componentDidUpdate() {
    if (this.state.selectedClientId !== this.props.selectedClientId) {
      // Actualitzem l'estat perque no entri en bucle
      this.setState({ selectedClientId: this.props.selectedClientId });
      // Recollim les dades de la BD de les sessions i les posem a sessionsData
      let aux = {};
      let auxArray = [];
      let that = this;
      let key = 1;
      firebase
        .firestore()
        .collection("SESSIONS")
        .where("ClientId", "==", this.props.selectedClientId)
        .get()
        .then((sessions) => {
          sessions.forEach((session) => {
            aux = session.data();
            aux["Date"] = new Date(aux["Date"]);
            aux["SessionId"] = session.id;
            aux["key"] = key;
            key++;
            auxArray.push(aux);
          });

          auxArray = this.sortArrayByDate(auxArray);

          auxArray.forEach((session, key) => {
            session["key"] = key;
          });

          that.setState({
            sessionsData: auxArray,
          });
        });
    }
  }

  sortArrayByDate = (auxArray) => {
    let day;
    let month;
    auxArray.sort((a, b) => b.Date - a.Date);
    auxArray.forEach((value, i) => {
      day = ("0" + value.Date.getDate()).slice(-2);
      month = ("0" + (value.Date.getMonth() + 1)).slice(-2);
      auxArray[i].Date = `${day}/${month}/${value.Date.getFullYear()}`;
    });

    return auxArray;
  };

  editSession = (field) => {
    this.props.editSessionInfo(field.SessionId);
    this.setState({
      editSession: true,
    });
  };

  deleteSession = (field) => {
    Swal.fire({
      title: "Estas segura que vols eliminar aquesta sessió?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#3085d6",
      confirmButtonText: "Si",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        let auxArray = [...this.state.sessionsData];
        auxArray.splice(field.key, 1);
        // Array keys update
        auxArray.forEach((session, key) => {
          session["key"] = key;
        });
        firebase
          .firestore()
          .collection("SESSIONS")
          .doc(field.SessionId)
          .delete()
          .then(function () {
            console.log("Document successfully deleted!");
            Swal.fire({
              icon: "success",
              title: "Sessió eliminada amb exit",
              showConfirmButton: false,
              timer: 1500,
            });
          })
          .catch(function (error) {
            console.error("Error removing document: ", error);
          });
        this.setState({
          sessionsData: auxArray,
        });
      }
    });
  };

  render() {
    return (
      <>
        {this.state.editSession && <Redirect to="editsession" />}
        <Table columns={this.columns} dataSource={this.state.sessionsData} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
  };
};

TaulaSessions.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  editSessionInfo: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { editSessionInfo })(TaulaSessions);
