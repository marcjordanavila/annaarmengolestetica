import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import firebase from "../../firebase";
import { Link } from "react-router-dom";

import TaulaSessions from "../taulaSessions/taulaSessions";
import {
  setClients,
  clientInformation,
  setUserLogout,
} from "../../actions/generalActions";
import logo from "../../img/logo.png";

import "./principalPanel.css";
import DropDownMenu from "../dropDownMenu/dropDownMenu";

class PrincipalPanel extends Component {
  state = {
    selectedClient: "",
  };

  componentDidMount = () => {
    let clientsArray = [];
    let aux;
    let that = this;
    firebase
      .firestore()
      .collection("CLIENTS")
      .get()
      .then(function (clients) {
        clients.forEach((doc) => {
          aux = doc.data();
          aux["clientId"] = doc.id;
          clientsArray.push(aux);
        });
        clientsArray.sort((a, b) => {
          if (a.Name < b.Name) {
            return -1;
          }
          if (a.Name > b.Name) {
            return 1;
          }
          return 0;
        });
        that.props.setClients(clientsArray);
      });
  };

  selectedClient = (clientId) => {
    this.setState({
      selectedClient: clientId,
    });
    let clientInfo = {};
    this.props.generalReducer.clients.forEach((client) => {
      if (client.clientId === clientId) {
        clientInfo = client;
      }
    });
    this.props.clientInformation(clientInfo);
  };

  logout = () => {
    var that = this;
    firebase
      .auth()
      .signOut()
      .then(function () {
        that.props.setUserLogout();
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  render() {
    return (
      <div className="container">
        <div className="col-12 mt-5">
          <div className="row justify-content-center">
            <img src={logo} alt="logo" className="w-7" />
          </div>
          <div className="row mt-4 justify-content-center">
            <div className="col-1 text-center d-flex align-items-center justify-content-center">
              Client:
            </div>
            <div className="col-5">
              <DropDownMenu
                options={this.props.generalReducer.clients}
                onSelect={this.selectedClient}
              />
            </div>
            <div className="col-2 text-center">
              <Link
                to="/addsession"
                onClick={() => this.selectedClient(this.state.selectedClient)}
              >
                <button className="btn btn-primary">Afegir sessió </button>
              </Link>
            </div>

            <div className="col-2 text-center">
              <Link to="/newclient">
                <button className="btn btn-primary">Nou Client</button>
              </Link>
            </div>
            <div className="col-2 text-center">
              <Link
                to="/editclient"
                onClick={() => this.selectedClient(this.state.selectedClient)}
              >
                <button className="btn btn-primary">Editar Client</button>
              </Link>
            </div>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-12 mt-5 mb-4">
            <p>Observacions:</p>
            <div className="pt-3 border rounded text-center ">
              <p>{this.props.generalReducer.selectedClient.Observations}</p>
            </div>
          </div>
        </div>
        <br />
        <TaulaSessions selectedClientId={this.state.selectedClient} />
        <div className="row justify-content-center">
          <div className="col-12 mt-5 mb-4 text-center">
            <button className="btn btn-primary" onClick={this.logout}>
              Tancar sessió
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
  };
};

PrincipalPanel.propTypes = {
  generalReducer: PropTypes.object.isRequired,
  setClients: PropTypes.func.isRequired,
  clientInformation: PropTypes.func.isRequired,
  setUserLogout: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  setClients,
  clientInformation,
  setUserLogout,
})(PrincipalPanel);
