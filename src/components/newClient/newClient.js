import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";
import firebase from "../../firebase";
import Swal from "sweetalert2";

import "../../generalStyle.css";
import "./newClient.css";

class NewClient extends Component {
  state = {
    nomClient: "",
    telClient: "",
    mailClient: "",
    observacions: "",
    added: false,
  };

  handleOnClickCrear = () => {
    let that = this;
    firebase
      .firestore()
      .collection("CLIENTS")
      .add({
        Email: this.state.mailClient,
        Name: this.state.nomClient,
        Observations: this.state.observacions,
        Phone: this.state.telClient,
      })
      .then(() => {
        that.setState({
          added: true,
        });
        Swal.fire({
          icon: "success",
          title: "Client inserit amb èxit",
          showConfirmButton: false,
          timer: 3000,
        });
      });
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  render() {
    return (
      <div className="container">
        <div className="col-12 mt-5">
          <div className="row">
            {this.state.added && <Redirect to="principalPanel" />}
            <div className="col-12">
              <h3 className="text-center">Informació client/a</h3>
            </div>
          </div>
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Nom:</div>
            <div className="col-10 mt-3">
              <input
                type="text"
                name="nomClient"
                onChange={this.handleOnChange}
                className="form-control"
              />
            </div>
          </div>
          {/*<div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Teléfon:</div>
            <div className="col-10 mt-3">
              <input
                type="text"
                name="telClient"
                onChange={this.handleOnChange}
                className="form-control"
              />
            </div>
    </div>*/}
          <div className="row">
            <div className="col-2 mt-3 d-flex align-items-center">Correu:</div>
            <div className="col-10 mt-3">
              <input
                type="text"
                name="mailClient"
                onChange={this.handleOnChange}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <p className="col-12 mt-4">Observacions:</p>
          </div>
          <div className="row">
            <div className="col-12">
              <textarea
                name="observacions"
                onChange={this.handleOnChange}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-6 text-center mt-4">
              <Link to="principalpanel">
                <button className="btn btn-danger">Cancel·lar</button>
              </Link>
            </div>
            <div className="col-6 text-center mt-4">
              <button
                className="btn btn-primary"
                onClick={this.handleOnClickCrear}
              >
                Crear client
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    generalReducer: state.generalReducer,
  };
};

NewClient.propTypes = {
  generalReducer: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, null)(NewClient);
