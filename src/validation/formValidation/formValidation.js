export const formValidation = (field, value) => {
  //expresions regulars
  const nameValidation = /^[a-zA-Z ]{3,25}$/;
  const phoneValidation = /^[0-9]{9}$/;
  const emailValidation = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})$/;
  const observationsValidation = /^[a-zA-Z0-9]{0,50}$/;
  switch (field) {
    case "nomClient":
      if (nameValidation.test(value)) return true;
      break;
    case "telClient":
      if (phoneValidation.test(value)) return true;
      break;
    case "mailClient":
      if (emailValidation.test(value)) return true;
      break;
    case "observacions":
      if (observationsValidation.test(value)) return true;
      break;
    default:
      break;
  }
  return false;
};
