import * as actionTypes from "../actions/actions";

const initialState = {
  clients: [],
  selectedClient: {},
  selectedSession: "",
  skinTypes: [],
  hairThikness: [],
  tanDegrees: [],
  zones: [],
  userLogged: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.CLIENT_INFO:
      return {
        ...state,
        selectedClient: action.payload,
      };
    case actionTypes.EDIT_CLIENT_INFO:
      return {
        ...state,
        selectedClient: action.payload,
      };
    case actionTypes.EDIT_SESSION_INFO:
      return {
        ...state,
        selectedSession: action.payload,
      };
    case actionTypes.USER_LOGGED:
      return {
        ...state,
        userLogged: true,
      };
    case actionTypes.USER_LOGOUT:
      return {
        ...state,
        userLogged: false,
      };
    case actionTypes.SET_CLIENTS:
      return {
        ...state,
        clients: action.payload,
      };
    case actionTypes.SET_HAIR_THICKNESS:
      return {
        ...state,
        hairThikness: action.payload,
      };
    case actionTypes.SET_SKIN_TYPES:
      return {
        ...state,
        skinTypes: action.payload,
      };
    case actionTypes.SET_TAN_DEGREES:
      return {
        ...state,
        tanDegrees: action.payload,
      };
    case actionTypes.SET_ZONES:
      return {
        ...state,
        zones: action.payload,
      };
    default:
      return state;
  }
}
